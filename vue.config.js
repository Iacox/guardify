module.exports = {
  pwa: {
    name: 'GuardiFy',
    themeColor: '#FFF',
    msTileColor: '#FFF',
    appleMobileWebAppStatusBarStyle: 'black',
    manifestOptions: {
      name: 'GuardiFy Auth App',
      short_name: 'GuardiFy',
      description: 'Authentication service',
      display: 'fullscreen',
      theme_color: '#FFF',
      background_color: '#FFF',
      icons: [],
      start_url: '.',
    },
  },
  publicPath: '/',
}
