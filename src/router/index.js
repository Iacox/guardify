import Vue from 'vue'
import VueRouter from 'vue-router'
// import store from '@/store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'getting-started',
    meta: {
      layout: 'Main',
      requiresAuth: false,
    },
    component: () => import('@/views/GettingStarted'),
  },
  {
    path: '/activity',
    name: 'activity',
    meta: {
      layout: 'Main',
      requiresAuth: false, // переключить в true
    },
    component: () => import('@/views/Activity'),
  },
  {
    path: '/connections',
    name: 'connections',
    meta: {
      layout: 'Main',
      requiresAuth: false, // переключить в true
    },
    component: () => import('@/views/Connections'),
  },
  {
    path: '/attack-protection',
    name: 'attack-protection',
    meta: {
      layout: 'Main',
      requiresAuth: false, // переключить в true
    },
    component: () => import('@/views/AttackProtection'),
  },
  {
    path: '/monitoring',
    name: 'monitoring',
    meta: {
      layout: 'Main',
      requiresAuth: false, // переключить в true
    },
    component: () => import('@/views/Monitoring'),
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
})

// router.beforeEach((to, from, next) => {
//   if (to.matched.some(record => record.meta.requiresAuth)) {
//     // Добавить проверку на аутентификацию
//     if (store.getters.getAuthInfo.name && store.getters.getAuthInfo.pass) {
//       next()
//       return
//     }
//     next('/login')
//   } else {
//     next()
//   }
// })

export default router
