import Vue from 'vue'
import App from './App.vue'
// import './registerServiceWorker'
import router from './router'
import store from './store'
import meta from 'vue-meta'
import tooltipDirective from './directives/tooltip.directive'
import Preloader from './components/Preloader'
import 'materialize-css/dist/js/materialize.min'

Vue.config.productionTip = false

// Vue.use({
//   install(Vue) {
//     Vue.prototype.$materialize = M
//   },
// })
Vue.use(meta)
Vue.directive('tooltip', tooltipDirective)
Vue.component('Preloader', Preloader)

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
