export default {
  actions: {
    async fetchCommonRequest({ dispatch }, payload) {
      return await dispatch('getFetch', { payload })
    },
    async getFetch({ rootState }, { payload, method, headers }) {
      try {
        const request = await fetch('/crm.php', {
          method: method ?? 'POST',
          headers: {
            'Content-Type': 'application/json',
            ...(headers ?? {}),
          },
          body: JSON.stringify({
            // Добавить параметры юзера из rootState,
            rootState, // <-- там хранить
            ...(payload ?? {}),
          }),
        })

        const data = await request.text()

        try {
          return await JSON.parse(data)
        } catch (e) {
          return { result: 'bad' }
        }
      } catch (e) {
        return { result: 'bad' }
      }
    },
  },
}
