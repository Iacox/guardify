export default {
  state: {
    sideBarVisible: false,
  },
  mutations: {
    openSideBar(state) {
      state.sideBarVisible = true
    },
    closeSideBar(state) {
      state.sideBarVisible = false
    },
  },
  getters: {
    sideBarOpened(state) {
      return state.sideBarVisible
    },
  },
}
