import Vue from 'vue'
import Vuex from 'vuex'
import sidebar from './modules/settings/sidebar'
import fetch from './modules/settings/fetch'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    fetch,
    sidebar,
  },
  state: {
    userInfo: {
      name: 'Portulak',
    },
  },
  mutations: {
    updUserInfo(state, data) {
      state.userInfo = data
    },
  },
  actions: {
    async fetchUserInfo({ commit, dispatch }, unit) {
      let payload = {
        act: 'get_product_partner_offer',
        ...unit,
      }
      let data = await dispatch('getFetch', { payload })

      if (Array.isArray(data)) commit('updProductItemInfo', data[0][0])

      return { result: 'ok' }
    },
  },
  getters: {
    getUserInfo(state) {
      return state.userInfo
    },
  },
})
