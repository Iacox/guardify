export default {
  bind(el, { modifiers, value }, vnode) {
    const position = fstItemArr(Object.keys(modifiers)) ?? value.position ?? defaultPosition
    const html = typeof value === 'object' && value.html ? value.html : value
    prepareBinding(el, { value: html, position }, vnode)
  },
  update(el, { modifiers, value }, vnode) {
    const position = fstItemArr(Object.keys(modifiers)) ?? value.position ?? defaultPosition
    const html = typeof value === 'object' && value.html ? value.html : value
    prepareBinding(el, { value: html, position }, vnode)
  },
  unbind(el) {
    prepareUnbinding(el)
  },
}

const prepareBinding = (el, { value, position }, vnode) => {
  el.onmouseover = () => {
    if (+vnode.elm.dataset.timeout) clearTimeout(+vnode.elm.dataset.timeout)
    const tooltip = M.Tooltip.getInstance(el)
    if (!tooltip)
      tooltipInit(el, {
        html: value,
        position: position,
      })
    vnode.elm.dataset.timeout = setTimeout(() => {
      tooltipDestroy(el)
    }, 4000).toString()
  }
  el.onmouseleave = () => {
    if (+vnode.elm.dataset.timeout) clearTimeout(+vnode.elm.dataset.timeout)
    vnode.elm.dataset.timeout = setTimeout(() => {
      tooltipDestroy(el)
    }, 400).toString()
  }
}
const prepareUnbinding = el => {
  el.onmouseover = undefined
  el.onmouseleave = undefined
}
const tooltipInit = (el, value) => {
  let default_options = {}

  if (typeof value === 'string') default_options.html = value
  else default_options = { ...value }

  M.Tooltip.init(el, default_options)
}
const tooltipDestroy = el => {
  const tooltip = M.Tooltip.getInstance(el)
  tooltip?.destroy()
}
const fstItemArr = array => (Array.isArray(array) && array.length ? array[0] : null)
const defaultPosition = 'bottom'
